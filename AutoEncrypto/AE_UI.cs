﻿using System;
using System.IO;
using System.Threading;
using System.Windows.Forms;
using Microsoft.VisualBasic;

namespace AutoEncrypto
{
    public partial class AE_UI : Form
    {
        private Thread oThread, xThread;
        private EncryptionService es, ds;
        private string temp;


        public AE_UI()
        {
            temp = GetTemporaryDirectory(); 
            InitializeComponent();
            this.MaximizeBox = false;
            textBox2.PasswordChar = '*';
            es = new EncryptionService();
            ds = new EncryptionService();
            oThread = new Thread(new ThreadStart(es.Start));
            xThread = new Thread(new ThreadStart(ds.Decrypt));
            progressBar1.Value = 0;
            progressBar1.Minimum = 0;
            progressBar1.Maximum = 100;
            progressBar1.Style = ProgressBarStyle.Marquee;
            progressBar1.Visible = false;
            
            //oThread.Start();

        }

        private void label1_Click(object sender, EventArgs e)
        {
            
        }

        private void button1_Click(object sender, EventArgs e)
        {

            if (oThread.IsAlive)
            {
                DialogResult result;
                if (!string.IsNullOrEmpty(es.getCurrentFile()))
                {
                    result = MessageBox.Show("AutoEncrypto is in the middle of encrypting " + es.getCurrentFile() + " which seems to be taking a while. Stop the Service nevertheless?", "Stop Service Mid-Encryption?", MessageBoxButtons.YesNo, MessageBoxIcon.Stop);
                } else
                {
                    result = MessageBox.Show("Stop Monitoring & Auto-Encrypting the files in " + es.getFolder() + "?", "Stop Encryption Service?", MessageBoxButtons.YesNo,MessageBoxIcon.Warning);
                }
                if (result == DialogResult.Yes)
                {
                    oThread.Abort();
                    button1.Text = "Start";
                    button2.Enabled = button3.Enabled = true;
                    textBox2.ReadOnly = false;
                    progressBar1.Visible = false;
                }            
            }
            else {
                if (string.IsNullOrEmpty(textBox2.Text) || string.IsNullOrEmpty(textBox1.Text))
                {
                    MessageBox.Show("Encryption Key & Target Folder Required", "", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                else
                {
                    es.setFolder(@"" + textBox1.Text);
                    button2.Enabled = button3.Enabled = false;
                    if (textBox2.Text != null)
                        es.setPassword(textBox2.Text);
                    textBox2.ReadOnly = true;
                    oThread = new Thread(new ThreadStart(es.Start));
                    oThread.Start();
                    button1.Text = "Stop";
                    progressBar1.Visible = true;

                }
            }
            
        }

        private void AE_UI_Load(object sender, EventArgs e)
        {
            notifyIcon1.BalloonTipText = "AutoEncrypto is Running";
            notifyIcon1.BalloonTipTitle = "CS Lab Project";
            Visible = false;
        }

        private void AE_UI_Resize(object sender, EventArgs e)
        {
            if (FormWindowState.Minimized == WindowState)
            {
                this.Hide();
                ShowIcon = false;
                notifyIcon1.Visible = true;
                notifyIcon1.ShowBalloonTip(10);
            }
        }

        private void notifyIcon1_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            this.Show();
            ShowIcon = true;
            ShowInTaskbar = true;
            notifyIcon1.Visible = false;
            WindowState = FormWindowState.Normal;
        }

        private void AE_UI_FormClosed(object sender, FormClosedEventArgs e)
        {
            if(Directory.Exists(temp))
                Directory.Delete(temp, true);

            Application.Exit();
        }

        private void folderBrowserDialog1_HelpRequest(object sender, EventArgs e)
        {

        }

        private void button2_Click(object sender, EventArgs e)
        {
            folderBrowserDialog1.ShowDialog();
            textBox1.Text = folderBrowserDialog1.SelectedPath;
        }

        private void button3_Click(object sender, EventArgs e)
        {
            if(xThread.IsAlive)
            {
                MessageBox.Show("Currently decrypting " + ds.getCurrentFile(), "Decryption still in Progress...");
            }
            else
            {
                string queryKey = Interaction.InputBox("Verify Encryption/Decryption Key", "Confirm Decryption Key", "");
                xThread = new Thread(new ThreadStart(ds.Decrypt));

                if (!string.IsNullOrEmpty(queryKey))
                {
                    ds.setPassword(queryKey);
                    ds.setFolder(@"" + textBox1.Text);
                    temp = GetTemporaryDirectory();
                    ds.setDFolder(temp);
                    xThread.Start();
                }
            }
        }

        public string GetTemporaryDirectory()
        {
            // in case folder closed prematurely delete all previously created & associated temporary folders
            foreach (string dir in Directory.GetDirectories(Path.GetTempPath(), "*_auto_encrypto"))
                Directory.Delete(dir, true);

            string tempDirectory = Path.Combine(Path.GetTempPath(), Path.GetRandomFileName() + "_auto_encrypto");
            return tempDirectory;
        }

        private void AE_UI_FormClosing(object sender, FormClosingEventArgs e)
        {
            oThread.Abort();
            xThread.Abort();
        }
    }
}
