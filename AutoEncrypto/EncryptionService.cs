﻿using System;
using System.Diagnostics;
using System.IO;
using System.Security.Cryptography;
using System.Text;
using System.Threading;
using System.Windows.Forms;


namespace AutoEncrypto
{
    class EncryptionService
    {
        private string password = "";
        private string folder;
        private string dFolder;
        private string passwordHash;
        string fileEncrypted, currentFile;

        public void Start()
        {
            StringBuilder sb = new StringBuilder();

            byte[] passHash = SHA256.Create().ComputeHash(
                    new UnicodeEncoding().GetBytes(password));

            foreach (byte b in passHash)
                sb.Append(b.ToString("x2"));
            passwordHash = sb.ToString();

            if (!Directory.Exists(folder))
                Directory.CreateDirectory(folder);

            startEncryptionService();
        }

        public void Decrypt()
        {
            StringBuilder sb = new StringBuilder();

            byte[] passHash = SHA256.Create().ComputeHash(
                    new UnicodeEncoding().GetBytes(password));

            foreach (byte b in passHash)
                sb.Append(b.ToString("x2"));
            passwordHash = sb.ToString();

            if (!Directory.Exists(dFolder))
                Directory.CreateDirectory(dFolder);

            startDecryptionService();
        }

        public string getCurrentFile()
        {
            return currentFile;
        }

        public string getHalfEncrypted()
        {
            return fileEncrypted;
        }
        public string getDFolder()
        {
            return dFolder;
        }

        public void setDFolder(string temp)
        {
            this.dFolder = temp;
        }

        public void setFolder(string folder)
        {
            this.folder = folder;
        }

        public string getPassword()
        {
            return password;
        }

        public string getFolder()
        {
            return folder;
        }

        public void setPassword(string password)
        {
            this.password = password;
        }

        public void startEncryptionService()
        {
            while (true)
            {
                
                if (Directory.Exists(folder))
                {
                    string[] search = Directory.GetFiles(folder, "*.*");

                    foreach (string file in search)
                    {
                        if (!Path.GetExtension(file).Equals(".auto_encrypto") && !Path.GetExtension(file).Equals(".temp_encrypt")) 
                        {
                            currentFile = file;
                            encryptFile(file);
                            currentFile = null;
                        }
                    }
                }

            }
        }

        public void startDecryptionService()
        {
            bool found = false;
                if (Directory.Exists(folder))
                {
                    string[] search = Directory.GetFiles(folder, "*.*");
                
                    foreach (string file in search)
                    {
                        
                        string no_encrypto = Path.GetFileNameWithoutExtension(file);

                        if (Path.GetExtension(file).Equals(".auto_encrypto"))
                        {
                            if (Path.GetExtension(no_encrypto).Equals("." + passwordHash))
                            {
                                currentFile = file;
                                decryptFile(file);
                                found = true;
                                currentFile = null;
                        }
                            //File.Delete(file);
                        }
                    }
                if (found)
                {
                    MessageBox.Show("All files that were encrypted using the specified password have been decrypted", "Decryption Complete");
                    Process.Start(dFolder);
                }
                else
                {
                    MessageBox.Show("No file in the folder was encrypted using that key.", "Incorrect Password!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }

            }
        }

        public void encryptFile(string file)
        {
            //byte[] bytesToBeEncrypted = File.ReadAllBytes(file);
            UnicodeEncoding ue = new UnicodeEncoding();
            byte[] passwordBytes = ue.GetBytes(password);

            // Hash the password with SHA256
            //passwordBytes = SHA256.Create().ComputeHash(passwordBytes);
            

            // string fileEncrypted = Path.GetDirectoryName(file) + "\\" + Path.GetFileNameWithoutExtension(file) + "_encrypted" + Path.GetExtension(file);
            fileEncrypted = file + "." + passwordHash + ".temp_encrypt";
            // remove custom extension
            //string fileEncrypted = Path.GetDirectoryName(file) + "\\" + Path.GetFileNameWithoutExtension(file);
            string finishedFileName = Path.GetDirectoryName(fileEncrypted) + "\\" + Path.GetFileNameWithoutExtension(fileEncrypted) + ".auto_encrypto";

            using (FileStream fileCrypt = new FileStream(fileEncrypted, FileMode.Create))
            {
                using (AesManaged encrypt = new AesManaged())
                {
                    //encrypt.Padding = PaddingMode.None;
                    try
                    {
                        
                        Console.WriteLine("Encrypting: " + file);
                        
                        using (CryptoStream cs = new CryptoStream(fileCrypt, encrypt.CreateEncryptor(passwordBytes, passwordBytes), CryptoStreamMode.Write))
                        {
                            using (FileStream fileInput = new FileStream(file, FileMode.Open))
                            {
                                encrypt.KeySize = 256;
                                encrypt.BlockSize = 128;
                                int data;
                                while ((data = fileInput.ReadByte()) != -1)
                                {
                                    cs.WriteByte((byte)data);   
                                }
                                fileInput.Close();
                                File.Delete(file);
                            }
                            cs.Close();
                        }
                    }
                    catch (IOException e) { Console.WriteLine(e.Message); }
                    catch (ThreadAbortException ta)
                    {
                        File.Delete(fileEncrypted);
                        File.Delete(finishedFileName);
                        MessageBox.Show(file + " Remains unencrypted.", "Stop Service Mid-Encryption", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        Console.WriteLine("Thread Aborted:", ta.Message);
                    }
                    catch (Exception e)
                    {
                        MessageBox.Show(e.Message, "Unable to Read File", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }

                }
                fileCrypt.Close();
                
                File.Delete(finishedFileName);
                File.Move(fileEncrypted, finishedFileName);
            }
        }

        public void decryptFile(string file)
        {
            //byte[] bytesToBeEncrypted = File.ReadAllBytes(file);
            UnicodeEncoding ue = new UnicodeEncoding();
            byte[] passwordBytes = ue.GetBytes(password);

            // Hash the password with SHA256
            // byte[] passwordHash = SHA256.Create().ComputeHash(passwordBytes);

            // string fileDecrypted = Path.GetDirectoryName(file) + "\\" + Path.GetFileNameWithoutExtension(file) + "_encrypted" + Path.GetExtension(file);
            //string fileDecrypted = file + ".auto_encrypto";
            // remove custom extension
            string fileDecrypted = Path.GetFileNameWithoutExtension(file);
            fileDecrypted = dFolder + @"\" + Path.GetFileNameWithoutExtension(fileDecrypted);

            using (FileStream fileCrypt = new FileStream(file, FileMode.Open))
            {
                
                using (AesManaged decrypt = new AesManaged())
                {
                    decrypt.Padding = PaddingMode.None;
                    try
                    {
                        using (CryptoStream cs = new CryptoStream(fileCrypt, decrypt.CreateDecryptor(passwordBytes, passwordBytes), CryptoStreamMode.Read))
                        {
                            using (FileStream fileOutput = new FileStream(fileDecrypted, FileMode.Create))
                            {
                                //decrypt.KeySize = 256;
                                decrypt.BlockSize = 128;
                                int data;
                                while ((data = cs.ReadByte()) != -1)
                                    fileOutput.WriteByte((byte)data);
                                fileOutput.Close();
                            }
                            cs.Close();
                        }
                    }
                    catch (Exception e)
                    {
                        throw new Exception(e.Message, e);
                    }

                }
                fileCrypt.Close();
            }
        }

    }
}
