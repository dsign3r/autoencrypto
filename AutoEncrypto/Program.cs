﻿using System;
using System.Windows.Forms;
using System.Threading;

namespace AutoEncrypto
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            //displayUI();
            //Thread aThread = new Thread(new ThreadStart(displayUI));
            //aThread.Start();
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new AE_UI());
        }

        private static void displayUI()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new AE_UI());
        }

    }
}
